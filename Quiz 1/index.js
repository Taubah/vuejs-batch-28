//no 1

function next_date(tanggal, bulan, tahun) {
  var objectHari = { year: "numeric", month: "long", day: "numeric" };
  const hariBaru = new Date(tahun, bulan - 1, tanggal + 1);
  console.log(hariBaru.toLocaleDateString("id", objectHari));
}

var tanggal = 28;
var bulan = 2;
var tahun = 2021;
next_date(tanggal, bulan, tahun);

//No 2
function jumlah_kata(kalimat) {
  return kalimat.split(" ").length;
}

var kalimat_1 = "Halo nama saya Taubah Fadja Lesmana";
var kalimat_2 = "Saya Babah";
console.log(jumlah_kata(kalimat_1));
console.log(jumlah_kata(kalimat_2));